### 0.3.2_1.8.5
Update the algorithm
Changes:
- The available pre-configured pipelines match the updated list from CPAC
- Send logging to output directory to avoid limits

### 0.3.1_1.8.0

Bug fix:

- api-key is read-only
- Download the entire BIDS directory (except diffusion) and run from any level
  
Enhancements:

- Quicker failure, if pipeline is not specified.

### 0.3.0_1.8.0

Enhancements:

- Upgrade to C-PAC 1.8.0

Documentation:

- Add docs folder
- Add docs/release_notes.md
