"""Test results.py"""
import os
from unittest.mock import patch

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_cpac import results


@patch("builtins.open")
@patch("fw_gear_cpac.results.os.chdir")
@patch(
    "fw_gear_cpac.results._find_and_move_results",
    return_value="My favorite path is the narrow one",
)
def test_results_zips_file(
    mock_analysis_dir, mock_chdir, mock_open, mock_context, mock_hierarchy
):
    """
    Does the method run all the way through when given returncodes == 0 as patch?
    """
    with patch("fw_gear_cpac.results.sp.run") as patch_run:
        patch_run.return_value.returncode = 0
        patch_run.return_value.stdout = "stdout"
        patch_run.return_value.stderr = "No problems"
        e_code = results.Zip_Results(mock_context, mock_hierarchy)
    mock_open.close()
    assert e_code == 0
    assert mock_analysis_dir.call_count == 1


@patch("builtins.open")
@patch("fw_gear_cpac.results.os.chdir")
@patch(
    "fw_gear_cpac.results._find_and_move_results",
    return_value="My favorite path is the narrow one",
)
def test_results_prints_stderrs(
    mock_analysis_dir, mock_chdir, mock_open, mock_context, mock_hierarchy
):
    """
    Can the stderrs be captured?
    Three commands should be executed to (1) build the dir tree html, (2) zip the
    C-PAC output files and (3) remove unzipped output. Each command has a returncode, stdout, and stderr
    """
    with patch("fw_gear_cpac.results.sp.run") as patch_run:
        patch_run.return_value.returncode = 1
        patch_run.return_value.stdout = "stdout"
        patch_run.return_value.stderr = "I found a problem"
        errs = results.Zip_Results(mock_context, mock_hierarchy)
    assert errs == 3


@patch("builtins.open")
@patch("fw_gear_cpac.results.os.chdir")
@patch("fw_gear_cpac.results.sp.run")
@patch(
    "fw_gear_cpac.results._find_and_move_results",
    side_effect=ValueError,
    return_value="My favorite path is the narrow one",
)
def test_results_throws_ValueError(
    mock_analysis_dir,
    mock_run,
    mock_chdir,
    mock_open,
    mock_context,
    mock_hierarchy,
    caplog,
):
    """
    IF the filepath returned from _find_and_move_results
    is faulty, does Zip_Results throw an error?
    """

    results.Zip_Results(mock_context, mock_hierarchy)
    assert mock_run.call_count == 0
    assert "No results" in caplog.messages[0]


@pytest.mark.parametrize(
    "glob_result, expected_count", [(["a", "list", "of", "files"], 1), ([], 0)]
)
@patch("fw_gear_cpac.results.shutil.move")
@patch("fw_gear_cpac.results.set_output_path")
@patch("fw_gear_cpac.results.find_common_path", return_value="This/runs/fine")
@patch("fw_gear_cpac.results.glob")
def test_results_finds_dirs(
    mock_glob,
    mock_path,
    mock_output,
    mock_mv,
    glob_result,
    expected_count,
    mock_context,
    mock_hierarchy,
):
    """
    Is a filepath the output of manipulation in _find_and_move_results?
    """
    mock_context.config.update({"gear-dry-run": True})
    mock_glob.return_value = glob_result
    with patch("fw_gear_cpac.results.sp.run"):
        results._find_and_move_results(mock_context, mock_hierarchy)

    assert mock_path.call_count == expected_count
    assert mock_output.call_count == expected_count
    assert mock_mv.call_count == expected_count
