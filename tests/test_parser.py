"""Test parser.py"""
from unittest.mock import MagicMock, PropertyMock, patch

import pytest

from fw_gear_cpac import parser


@patch("fw_gear_cpac.parser._gather_cpac_input_files")
@patch("fw_gear_cpac.parser._convert_cpac_commands")
def test_parser_checks_pipeline(mock_convert, mock_gather, mock_context):
    """When no pipeline is provided and no preconfigured pipeline
    is chosen, the code should exit."""
    mock_context.inputs = {}

    parser.parse_config(mock_context)
    assert mock_convert.called
    assert mock_gather.called


@pytest.mark.parametrize(
    "preconfig, pipeline, expected",
    [("I am here", None, True), (None, "Exxon_mobile", False), (None, None, False)],
)
def test_check_pipeline_specified_is_true(
    preconfig, pipeline, expected, mock_context, caplog
):
    """
    If preconfig or pipeline_file are specified, then the gear should be allowed to
    continue processing.
    """
    if preconfig:
        mock_context.config.update({"preconfig": preconfig})
    if pipeline:
        mock_context.config_json.update({"inputs": pipeline})
    test_val = False
    parser._check_pipeline_specified(mock_context)
    if "preconfig" in mock_context.config:
        test_val = True
    assert test_val == expected


def test_parser_gathers_inputs(mock_context):
    mock_context.config_json["inputs"].update({"pipeline_file": "some_yaml"})
    input_files = parser._gather_cpac_input_files(mock_context)
    assert len(input_files) == 1


@patch("fw_gear_cpac.parser.cpac_validation.validate_params")
def test_convert_cpac_commands_validates(mock_validate, mock_context):
    """
    Does the method parse the config args and send them to be validated?
    Covers _convert_cpac_commands.
    """
    mock_context.config.update({"extra_arg": "have a nice day"})
    print(f"Does this look like context? {mock_validate}")
    gear_args = parser._convert_cpac_commands(mock_context)
    assert mock_validate.call_count == 1
    # gear-dry-run should be filtered along with others in the blacklist
    assert len(gear_args) == 2


@pytest.mark.parametrize(
    "SCA_opt",
    [("seed_based_correlation_atlas"), ("seed_based_correlation_analysis_type")],
)
@patch("fw_gear_cpac.parser.cpac_validation.validate_params")
def test_convert_cpac_commands_exits(mock_validate, SCA_opt, mock_context):
    """
    Does the method parse the config args and send them to be validated?
    Covers _convert_cpac_commands.
    """
    mock_context.config.update(
        {
            "seed_based_correlation_atlas": "random",
            "seed_based_correlation_analysis_type": "also_random",
        }
    )
    mock_context.config.pop(SCA_opt)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        parser._convert_cpac_commands(mock_context)
    assert pytest_wrapped_e.type == SystemExit


@pytest.mark.parametrize(
    "level, value, expected_label, expected_dir",
    [
        ("session", "one", "42_one", "fake/dir/to/bids/42/one"),
        ("subject", "", "42", "fake/dir/to/bids/42"),
    ],
)
@patch("fw_gear_cpac.parser.makedirs")
def test_refine_level_based_inputs_updates(
    fake_make, level, value, expected_label, expected_dir, mock_hierarchy
):
    if level == "session":
        mock_hierarchy["session_label"] = value
    else:
        mock_hierarchy.pop("session_label")

    updated_hierarchy = parser.refine_level_based_inputs(mock_hierarchy)
    assert updated_hierarchy["input_data_dir"] == expected_dir
    assert updated_hierarchy["analysis_label"] == expected_label
