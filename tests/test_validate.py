"""Test utils/validate.py"""
from unittest import TestCase
from unittest.mock import MagicMock, PropertyMock, patch

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_cpac import cpac_validation


@patch("fw_gear_cpac.cpac_validation.psutil.cpu_count", return_value=8)
def test_validate_params_tooMuchMem(mock_cpu_count, mock_context):
    """
    Multiple system configuration cases are tested with validate_params.
    Extensive testing is done in BIDS-mriqc; however, the present test suite
    uses a different mocking strategy and will only seek to test that configs
    are being checked.
    n_cpus should call cpu_count three times
    mem_gb should call virtual_memory twice if arg given is 1 TB
    mem_mb should call virtual_memory twice for a negative
    """
    params = {"n_cpus": 64, "mem_gb": 1000}
    with patch("fw_gear_cpac.cpac_validation.psutil.virtual_memory") as mock_vmem:
        mock_vmem.return_value.total = 1024**3 * 10
        cpac_validation.validate_params(params)
    assert mock_vmem.call_count == 3
    assert mock_cpu_count.call_count == 3


@patch("fw_gear_cpac.cpac_validation.psutil.cpu_count", return_value=8)
def test_validate_params_memInMB(mock_cpu_count, mock_context):
    """
    Multiple system configuration cases are tested with validate_params.
    Extensive testing is done in BIDS-mriqc; however, the present test suite
    uses a different mocking strategy and will only seek to test that configs
    are being checked.
    n_cpus should call cpu_count three times
    mem_gb should call virtual_memory twice for 1 TB
    mem_mb should call virtual_memory twice for a negative
    """
    params = {"n_cpus": 64, "mem_mb": (1024**3 * 100)}
    with patch("fw_gear_cpac.cpac_validation.psutil.virtual_memory") as mock_vmem:
        mock_vmem.return_value.total = 1024**3 * 10
        cpac_validation.validate_params(params)
    assert mock_vmem.call_count == 3
    assert mock_cpu_count.call_count == 3


@patch("fw_gear_cpac.cpac_validation.psutil.cpu_count", return_value=8)
def test_validate_params_negativesAndFewResources(mock_cpu_count, mock_context):
    """
    Multiple system configuration cases are tested with validate_params.
    Extensive testing is done in BIDS-mriqc; however, the present test suite
    uses a different mocking strategy and will only seek to test that configs
    are being checked.
    n_cpus should call cpu_count three times
    mem_gb should call virtual_memory twice for 1 TB
    mem_mb should call virtual_memory twice for a negative
    """
    params_list = [
        {"n_cpus": -1},
        {"mem_gb": -1},
        {"mem_gb": 4},
        {"mem_mb": -1},
        {"mem_mb": 10},
    ]
    with patch("fw_gear_cpac.cpac_validation.psutil.virtual_memory") as mock_vmem:
        mock_vmem.return_value.total = 1024**3 * 10
        for params in params_list:
            cpac_validation.validate_params(params)
    assert mock_vmem.call_count == 4
    assert mock_cpu_count.call_count == 2
