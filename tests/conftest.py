"""
Set up parameters for testing. Picked up by pytest automatically.
"""
import os.path as op
from unittest.mock import MagicMock

import pytest

import tests.test_main


@pytest.fixture
def mock_context(mocker):
    mocker.patch("flywheel_gear_toolkit.GearToolkitContext")
    base_dir = op.dirname(op.abspath(tests.test_main.__file__))
    gtk = MagicMock(
        autospec=True,
        config={
            "anat-only": True,
            "gear-dry-run": True,
            "gear-abort-on-bids-error": True,
            "test_config": "sunny_days",
        },
        config_json={"inputs": {"not_empty": "always_api_key_at_least"}},
        inputs={"api-key": "fake_key"},
        output_dir=op.join(base_dir, "test_output"),
    )
    return gtk


# Could also build hierarchy from flywheel_gear_toolkit.testing.hierarchy
@pytest.fixture
def mock_hierarchy(mocker):
    mocker.patch("flywheel_bids.utils.run_level.get_analysis_run_level_and_hierarchy")
    hierarchy = {
        "output_dir": "fake/output_dir",
        "bids_dir": "fake/dir/to/bids",
        "run_level": "session",
        "subject_label": "42",
        "session": "do_something",
        "session_dir": "I/was/built/in/another/method",
        "session_label": "special_task",
        "input_data_dir": "fake/dir/to/bids/special_task",
        "analysis_label": "Donkey Kong!",
    }
    return hierarchy
