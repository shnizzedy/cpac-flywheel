"""Parser module to parse gear config.json."""
import logging
import os.path as op
import sys
from collections import OrderedDict
from os import makedirs
from typing import Tuple

from flywheel_bids.utils import validate
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_cpac import cpac_validation

log = logging.getLogger(__name__)


# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(gear_context: GearToolkitContext) -> Tuple[str, str]:
    """Harvest inputs from the user and translate into parsed arguments for run.py

    Returns:
        input_filepaths (list): file paths for files to be passed to C-PAC
        gear_args (dict): options to be built into the C-PAC command

    """
    input_files = _gather_cpac_input_files(gear_context)
    gear_args = _convert_cpac_commands(gear_context)
    return input_files, gear_args


def refine_level_based_inputs(hierarchy: dict):
    """
    Determine session_label and input_data_dir based on the level from which the gear was launched.

    Args:
        hierarchy(dict): Flywheel information about the images being processed
    Returns
        hierarchy(dict)
    """
    if "session_label" in hierarchy:
        hierarchy["input_data_dir"] = op.join(
            hierarchy["bids_dir"],
            hierarchy["subject_label"],
            hierarchy["session_label"],
        )
        hierarchy["analysis_label"] = (
            hierarchy["subject_label"] + "_" + hierarchy["session_label"]
        )
        # Create a place for the output
        makedirs(hierarchy["input_data_dir"], exist_ok=True)
    else:
        hierarchy["input_data_dir"] = op.join(
            hierarchy["bids_dir"], hierarchy["subject_label"]
        )
        hierarchy["analysis_label"] = hierarchy["subject_label"]
    return hierarchy


def _check_pipeline_specified(gear_context):
    """
    Decide whether custom or stock pipeline yaml files are passed to the CPAC command.

    Either a custom-configured or preconfigured pipeline must be specified. If neither have been
    chosen, then terminate the gear.
    Returns
        Gear will exit, if no pipeline is specified.
    """
    if ("preconfig" in gear_context.config) or (
        "pipeline_file" in gear_context.config_json["inputs"]
    ):
        if "pipeline_file" in gear_context.config_json["inputs"]:
            # To avoid a pipeline collision, "override" the config parameter with the pipeline file
            # by removing the 'preconfig' option from the config field.
            gear_context.config.pop("preconfig", None)
            log.info("A custom pipeline is configured and gear can continue running.")
        else:
            log.info(f"Using {gear_context.config['preconfig']} as the pipeline.")
    else:
        log.warning(
            "No pipeline configuration specified. Gear will use CPAC's default pipeline.\n"
            "Seed-based correlation analysis scheduled, but not yet enabled."
        )
        # Send the algorithm no preconfig arg to trigger "default" pipeline
        gear_context.config.pop("preconfig", None)


def _gather_cpac_input_files(gear_context):
    """
    Report back the subset of manifest inputs that need to be passed on to the CPAC command.

    # During refactor, this method may be deemed unnecessary. Could simply return the values from
    the GTK context directly.

    Pipeline file (implemented)
    ROI file (future)
    - ROI file to be used for SCA

    Group file (future)

        - if the analysis container is a group one, then add path for "group_file"?
        - Leaving this subroutine here, even though it is a stub, since it is likely that we
        will need to support a Group file specification.
    Not supporting C-PAC option for "data_config_file", b/c we are enforcing BIDS specification is used
    and "data_config_file" is for non-BIDS' structures.
    Returns
        input_files (dict): commandline arg and filepath pairs
    """
    if gear_context.get_input_path("pipeline_file"):
        return {"pipeline_file": gear_context.get_input_path("pipeline_file")}
    else:
        return None


def _convert_cpac_commands(gear_context):
    """
    Scrub Flywheel-specific config options from args to be looped into CPAC command.

    Elements from the config.json are entered here to be filtered for C-PAC related commands and ordering.
    Items in the blacklist are either Flywheel settings to control other method behaviors or C-PAC items that
    must adhere to C-PAC positional order in the final command.
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
    Returns
        args (dict): parameter settings that can be passed to build_command_list
    """
    # Specify context config "blacklist" to avoid incorporating commands
    # into command line arguments
    black_list = [
        "debug",
        "test_config",
        "gear-dry-run",
        "gear-save-output-on-error",
        "gear-run-bids-validation",
        "gear-abort-on-bids-error",
        "seed_based_correlation_atlas",
        "seed_based_correlation_analysis_type",
    ]
    # use Ordered Dictionary to keep the order created.
    # Default in Python 3.6 onward
    args = OrderedDict()
    for arg, val in gear_context.config.items():
        if arg not in black_list:
            args[arg] = val

        # Since the configuration reported back by FW only includes populated fields,
        # arg will only match if the optional fields have been chosen
        if arg in (
            "seed_based_correlation_atlas",
            "seed_based_correlation_analysis_type",
        ):
            if (
                not (
                    set(
                        [
                            "seed_based_correlation_atlas",
                            "seed_based_correlation_analysis_type",
                        ]
                    ).intersection(gear_context.config.keys())
                )
                == 2
            ):
                sys.exit(
                    'Must define both "seed_based_correlation_atlas" and '
                    '"seed_based_correlation_analysis_type" in the configuration tab.'
                )

        # Only add SCA-related args once by checking for a specific key
        if arg == "seed_based_correlation_atlas":
            _set_sca_pieces(
                roi=gear_context.get_input_path(["ROI_file_for_SCA"]),
                atlas=val,
                method=gear_context.config["seed_based_correlation_analysis_type"],
                pipeline_yaml=gear_context.get_input_path["pipeline_file"],
            )
    cpac_validation.validate_params(args)
    return args


def _set_sca_pieces(roi=None, atlas=None, method=None, pipeline_yaml=None):
    """
    Next dev phase method. Mostly a placeholder for now, but will redefine the
    yaml file if all the inputs are given by the user. The yaml must be amended
    to actually run the SCA, as that option is available, but OFF by default.
    """
    if not all(roi, atlas, method):
        log.critical(
            "To run SCA, the ROI, atlas, and analysis type must all be specified.\n"
            "Please do so on the input and config tabs and try again."
        )
    else:
        log.info("SCA is not implemented and tested yet.")
        pass
        # if not pipeline_yaml:
        #     pipeline_yaml = find_the_default_pipeline_yaml
        # populate_yaml(pipeline_yaml, roi, atlas, method)
