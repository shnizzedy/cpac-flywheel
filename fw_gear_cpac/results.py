import logging
import os
import os.path as op
import shutil
import subprocess as sp
import sys
from glob import glob

log = logging.getLogger(__name__)


def Zip_Results(gear_context, hierarchy):
    """Cleanup, create manifest, create zipped results, move all results to the output directory
    This executes regardless of errors or exit status.
    Args
        gear_context (GeartoolkitContext): gear information
        hierarchy: Container labels and created directory structures
    Returns
        errs (int): The total number of errors encountered while zipping the analysis
        [Exception thrown if the output analysis or directory is missing]
    """
    os.chdir(gear_context.work_dir)

    # If the output/result.anat path exists, zip regardless of exit status
    # Clean input_file_basename to lack esc chars and extension info
    dest_dir = op.join(gear_context.output_dir, hierarchy["analysis_label"])
    dest_zip = dest_dir + ".zip"

    try:
        # CPAC buries the output deep in the work/bids directory, when moving
        # the processed files from its own "working" directory
        _find_and_move_results(gear_context, hierarchy)
        log.info(
            "Zipping " + hierarchy["analysis_label"] + " directory to " + dest_zip + "."
        )
        # For results with a large number of files, provide a manifest.
        # Capture the stdout/stderr in a file handle or for logging.
        file_tree = op.join(
            gear_context.output_dir,
            hierarchy["analysis_label"] + "_output_manifest.txt",
        )
        command0 = ["tree", "-shD", "-D", dest_dir]
        with open(file_tree, "w") as f:
            result0 = sp.run(command0, stdout=f)

        command1 = ["zip", "-r", dest_zip, dest_dir]
        result1 = sp.run(command1, stdout=sp.PIPE, stderr=sp.PIPE)

        # Clean up the folder
        command2 = ["rm", "-rf", dest_dir]
        result2 = sp.run(command2, stdout=sp.PIPE, stderr=sp.PIPE)

        returncode0 = result0.returncode
        returncode1 = result1.returncode
        returncode2 = result2.returncode
        errs = abs(returncode0) + abs(returncode1) + abs(returncode2)
        if errs != 0:
            stderr = []
            for rc in [result0, result1, result2]:
                stderr.append(rc.stderr)
        else:
            log.info("Files zipped cleanly.")
        return errs

    except Exception or SystemExit as e:
        log.debug(f"{e}")
        log.info("No results directory, " + dest_dir + ", to zip.")


def _find_and_move_results(gear_context, hierarchy):
    """
    Find the json files that are copied by CPAC from their 'working' directory.
    The jsons should be present in the analyzed directory, but may not be with
    the original scans, serving as a double-check that the output is being
    discovered, not the input.
    Args
        gear_context (GeartoolkitContext): gear information
        hierarchy: Container labels and created directory structures
    """
    log.debug("Locating results folder.")
    search_path = op.join(
        gear_context.work_dir,
        hierarchy["bids_dir"],
        hierarchy["input_data_dir"],
        "output",
        "**",
        "*.json",
    )
    possible_dirs = glob(
        search_path,
        recursive=True,
    )
    if possible_dirs:
        common_path = find_common_path(possible_dirs)
        new_path = set_output_path(gear_context.output_dir, hierarchy["analysis_label"])
        shutil.move(common_path, new_path)
    else:
        log.error(
            f"I am hoping you were doing a dry run, because there is no output.\nSearched {search_path}."
        )
        log.info("Contents of the search_path")
        command0 = [
            "tree",
            "-shD",
            "-D",
            op.join(gear_context.work_dir, hierarchy["bids_dir"]),
        ]
        log.info(sp.run(command0))
        if not gear_context.config.get("gear-dry-run", False):
            sys.exit(1)


def find_common_path(possible_dirs):
    # Determine the shared folder structure, which should be all but the
    # file names.
    common_path = op.commonpath(possible_dirs)

    # Provide correction for single entries
    if op.isfile(common_path) or len(possible_dirs) == 1:
        common_path = op.dirname(common_path)

    if op.isdir(common_path):
        log.info(f"Results are located in {common_path}")
    return common_path


def set_output_path(output_dir, analysis_label):
    """The working dir path is unnecessarily long, so this method shortens it and points to /flywheel/v0/output, so the results will be zipped."""
    new_path = op.join(output_dir, analysis_label)
    log.debug(f"New path: {new_path}")
    os.makedirs(new_path, exist_ok=True)
    return new_path
