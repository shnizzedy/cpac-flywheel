"""
CPAC has a few resource-related parameters that can be specified in the config tab.
This module checks the format and logical nature of those config inputs and processes
errors and warnings from legacy BIDS validation methods.
"""
import logging

import psutil

log = logging.getLogger(__name__)


def validate_params(params):
    """
    Input: gear context with parameters
    Attempts to correct any violations
    Args
        params (dict): Full set of passable parameters that will be used to build commands. Only computational
        keys are checked here for safe settings.
    Returns
        Logs warning on what may cause problems
    """

    # Check that cpus are between 0 and max number of cpus
    if "n_cpus" in params.keys():
        if params["n_cpus"] < 0:
            log.warning(
                "You can't have a negative number of cpus. \
                Assuming Max CPUs, %f.",
                psutil.cpu_count(),
            )
            params["n_cpus"] = psutil.cpu_count()
        elif params["n_cpus"] > psutil.cpu_count():
            log.warning(
                "You have requested more cpus than available \
                on the host system. Assuming Max CPUs, %f.",
                psutil.cpu_count(),
            )
            params["n_cpus"] = psutil.cpu_count()

    # Check that memory is between the default (6 GB) and the max system memory
    # 'mem_gb' takes precedence
    if "mem_gb" in params.keys():
        if params["mem_gb"] < 0:
            log.warning(
                "You have selected a negative amount of memory.\
                Assuming System Max of %f GB.",
                psutil.virtual_memory().total / (1024**3),
            )
            params["mem_gb"] = psutil.virtual_memory().total / (1024**3)
        elif params["mem_gb"] < 6:
            log.warning(
                "You have requested less than the default (6 GB). \
                This may cause a crash."
            )
        elif params["mem_gb"] > psutil.virtual_memory().total / (1024**3):
            log.warning(
                "You are trying to reserve more memory than the \
                system has available. \
                Setting to 90 percent of System maximum. %f GB.",
                psutil.virtual_memory().total * 0.9 / (1024**3),
            )
            params["mem_gb"] = psutil.virtual_memory().total * 0.9 / (1024**3)
    elif "mem_mb" in params.keys():
        if params["mem_mb"] < 0:
            log.warning(
                "You have selected a negative amount of memory. \
                Assuming System Max of %f MB.",
                psutil.virtual_memory().total / (1024**2),
            )
            params["mem_gb"] = psutil.virtual_memory().total / (1024**2)
        elif params["mem_mb"] < 6 * 1024:
            log.warning(
                "You have requested less than the default (6 GB). \
                This may cause a crash."
            )
        # BMS changed the following virtual_memory() call to specify ".total" 6.9.21
        elif params["mem_mb"] > psutil.virtual_memory().total / (1024**2):
            log.warning(
                "You are trying to reserve more memory than the system has \
                available. Setting to 90 percent of System maximum. %f GB.",
                psutil.virtual_memory().total * 0.9 / (1024**3),
            )
            params["mem_mb"] = psutil.virtual_memory().total * 0.9 / (1024**2)
